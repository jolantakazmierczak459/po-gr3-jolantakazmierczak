package pl.edu.uwm.wmii.jolantakazmierczak.laboratorium06;

public class zadanie1 {
    public static void main(String[] args){
        RachunekBankowy saver1 =  new RachunekBankowy(0,2000);
        RachunekBankowy saver2 =  new RachunekBankowy(0,3000);
        // stopa procentowa - 4
        saver1.setRocznaStopaProcentowa(4);
        saver1.obliczMiesieczneOdsetki();
        System.out.println(saver1.getSaldo());
        saver2.setRocznaStopaProcentowa(4);
        saver2.obliczMiesieczneOdsetki();
        System.out.println(saver2.getSaldo());

        //stopa procentowa - 5
        saver1.setRocznaStopaProcentowa(5);
        saver1.obliczMiesieczneOdsetki();
        System.out.println(saver1.getSaldo());
        saver2.setRocznaStopaProcentowa(5);
        saver2.obliczMiesieczneOdsetki();
        System.out.println(saver2.getSaldo());
    }
    static class RachunekBankowy {
        double rocznaStopaProcentowa;
        double saldo;

        private RachunekBankowy(double rocznaStopaProcentowa, double saldo) {
            this.rocznaStopaProcentowa = rocznaStopaProcentowa;
            this.saldo = saldo;
        }
        public double getSaldo()
        {
            return saldo;
        }
        public void obliczMiesieczneOdsetki(){
            this.saldo = this.saldo + ((saldo * rocznaStopaProcentowa) / 12);
        }
        public void setRocznaStopaProcentowa(double n){
            this.rocznaStopaProcentowa = n;
        }
    }

}



