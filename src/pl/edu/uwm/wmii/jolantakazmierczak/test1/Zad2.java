package pl.edu.uwm.wmii.jolantakazmierczak.test1;

import java.util.Scanner;


public class Zad2 {
    public static StringBuffer delete(String str, char c){
        StringBuffer s = new StringBuffer(str);
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == c)
                s.deleteCharAt(i);
        }
        return s;
    }
    public static void main(String[] arg) {
        System.out.println("Podaj text:");
        Scanner input = new Scanner(System.in);
        String text = input.nextLine();
        System.out.println(text);
        System.out.println("Podaj znak:");
        Scanner sc = new Scanner(System.in);
        char c = sc.next().charAt(0);

        System.out.println("Wynik: " + delete(text, c));
    }

}



//    napisz publiczną statyczną metodę delete (String str, char c),
//    która zwraca zmodyfikowany ciąg,usuwając całe wystąpienie c z wyjątkiem pierwszego wystąpienia