package pl.edu.uwm.wmii.jolantakazmierczak.laboratorium04;

import java.util.Scanner;

public class Zadanie1d {
    public static String repeat(String str, int n){
        String wynik;
        wynik = str.repeat(n);

        return wynik;
    }
    public static void main(String[] arg) {
        System.out.println("Podaj napis:");
        Scanner input = new Scanner(System.in);
        String napis = input.nextLine();

        System.out.println("Repeat: " + repeat(napis, 3));
    }
}
