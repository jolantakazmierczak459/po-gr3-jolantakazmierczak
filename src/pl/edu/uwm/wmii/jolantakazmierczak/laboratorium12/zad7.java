package pl.edu.uwm.wmii.jolantakazmierczak.laboratorium12;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class zad7 {
    public static void main(String[] args) {

        primes(100);

    }
    public static void primes(int n) {
        int m = 2;
        ArrayList<Integer> primes = new ArrayList<>();
        while (m < n) {
            primes.add(m);
            m++;
        }

        for (int i = 2; i < n; i++) {
            for (int j = 0; j < primes.size(); j++) {
                if (primes.get(j) % i == 0 && primes.get(j) != i) {
                    primes.remove(j);
                }
            }
        }
        System.out.println(primes);
    }
}
