package pl.imiajd.kazmierczak;

public class Zadanie4 {
    public static void main(String[] args){
        Osoba os = new Osoba();
        os.nazwisko = "Wieczorek";
        os.rokUrodzenia = 1989;
        Osoba.Student st = new Osoba.Student();
        st.kierunek = "Informatyka";
        Osoba.Nauczyciel na = new Osoba.Nauczyciel();
        na.pensja = 3000;

    }
    static class Osoba{
        private String nazwisko;
        private int rokUrodzenia;
        public Osoba(){
            this.nazwisko = nazwisko;
            this.rokUrodzenia = rokUrodzenia;
        }
        private  String getNazwisko(){ return nazwisko; }

        private int getRokUrodzenia(){ return rokUrodzenia; }



        static class Student extends Osoba{
            String kierunek;
            public Student(){
                this.kierunek = kierunek;
            }
            public String getKierunek(){ return kierunek; }

        }
        static class Nauczyciel extends Osoba{
            double pensja;
            public Nauczyciel(){
                this.pensja = pensja;
            }
            public double getPensja(){ return pensja; }

        }
    }
}


//    Utwórz klasę Osoba oraz dwie klasy, Student i Nauczyciel, które są podklasami klasy
//        Osoba, która ma dwa prywatne pola: nazwisko i rokUrodzenia. Klasa Student ma
//        dodatkowo pole kierunek, a klasa Nauczyciel pole pensja. Zdefiniuj wszystkie klasy,
//        konstruktory, metodę toString dla wszystkich klas oraz metody typu get. Napisz także
//        program testujący zdefiniowane klasy i metody.
