package pl.edu.uwm.wmii.jolantakazmierczak.laboratorium04;

import java.util.Scanner;

public class Zadanie1b {
    public static int countSubStr(String str, String subStr){
        String temp[] = str.split(" ");
        int count = 0;
        for (int i = 0; i < temp.length; i++) {
            if (subStr.equals(temp[i]))
                count++;
        }
        return count;
    }

    public static void main(String[] arg) {
//        System.out.println("Podaj napis:");
//        Scanner input = new Scanner(System.in);
//        String string = input.nextLine();
//
//
//        System.out.println("Podaj słowo:");
//        Scanner input2 = new Scanner(System.in);
//        String word = input2.nextLine();

        String string = "ala ma kota";
        String word = "ma";

        System.out.println("Ilość: " + countSubStr(string, word));
    }
}

