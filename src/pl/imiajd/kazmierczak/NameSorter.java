package pl.imiajd.kazmierczak;

import java.util.Comparator;

public class NameSorter implements Comparator<Komputer> {

    @Override
    public int compare(Komputer o1, Komputer o2) {
        return o1.getNazwa().compareTo(o2.getNazwa());
    }
}