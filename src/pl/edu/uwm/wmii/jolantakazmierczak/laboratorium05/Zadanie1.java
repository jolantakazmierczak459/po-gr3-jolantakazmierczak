package pl.edu.uwm.wmii.jolantakazmierczak.laboratorium05;

import java.util.ArrayList;

public class Zadanie1 {
    public static ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer> b) {
        ArrayList<Integer> wynik = new ArrayList<>();
        wynik.addAll(a);
        wynik.addAll(b);
        return wynik;
    }

    public static void main(String[] arg) {
        ArrayList<Integer> a = new ArrayList<>();
        a.add(1);
        a.add(2);
        a.add(3);
        ArrayList<Integer> b = new ArrayList<>();
        b.add(4);
        b.add(5);
        b.add(6);
        System.out.println(a);
        System.out.println(b);
        System.out.println("append: " + append(a, b));
    }

}