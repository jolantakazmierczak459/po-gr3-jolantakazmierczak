package pl.edu.uwm.wmii.jolantakazmierczak.laboratorium12;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;

public class zad4<T> {
    public static void main(String[] args)
    {
        LinkedList<String> list=new LinkedList<String>();

        list.add("Andrzej Maleszewski");
        list.add("Wojciech Kowalski");
        list.add("Jan Nowak");
        list.add("Marek Wieczorek");


//        test odwroc
        odwroc(list);

        Iterator<String> iterator=list.iterator();
        while(iterator.hasNext()){
            System.out.println(iterator.next());
        }

    }
    public static <T> void odwroc(LinkedList<T> lista){
        Collections.reverse(lista);
    }

}
