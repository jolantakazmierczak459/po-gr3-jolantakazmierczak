package pl.edu.uwm.wmii.jolantakazmierczak.laboratorium01.Zadanie1;

public class Zadanie1c {
    public static void main(String[] arg) {

        int n = 4;
        int[] tab = new int[n];
        for(int i= 0; i<n; i++){
            tab[i] = i-4;
        }

        int suma = 0;

        for(int j=0; j<n; j++){
            suma = suma + Math.abs(tab[j]);
        }

        System .out. println (suma);
    }
}