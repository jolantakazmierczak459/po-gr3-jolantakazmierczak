package pl.edu.uwm.wmii.jolantakazmierczak.laboratorium01.Zadanie1;

public class Zadanie1f {
    public static void main(String[] arg) {
        int n = 15;
        double[] tab = new double[n];
        for(int i=0; i<n; i++){
            tab[i] = i;
        }
        double suma = 0;
        for(int j=0; j<n; j++){
            double b = 2;
            suma = suma + Math.pow(tab[j], b);
        }

        System .out. println (suma);
    }
}

