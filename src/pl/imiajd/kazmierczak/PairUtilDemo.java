package pl.imiajd.kazmierczak;

public class PairUtilDemo {
    public static void main(String[] args)
    {
        Pair<String> test = new Pair();
        test.setFirst("pies");
        test.setSecond("kot");
        System.out.println("pierwszy = " + test.getFirst());
        System.out.println("drugi = " + test.getSecond());
        System.out.println("----------------------------");
        PairUtil.swap(test);
        System.out.println("pierwszy = " + test.getFirst());
        System.out.println("drugi = " + test.getSecond());
    }
}
