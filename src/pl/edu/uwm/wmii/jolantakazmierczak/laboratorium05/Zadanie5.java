package pl.edu.uwm.wmii.jolantakazmierczak.laboratorium05;

import java.util.ArrayList;


public class Zadanie5 {
    public static void reverse(ArrayList<Integer> a) {
        ArrayList<Integer> temp = new ArrayList<>();
        temp.addAll(a);
        int rozmA = a.size();
        a.clear();
        for (int i = 0; i < rozmA; i++)
            a.add(temp.get(rozmA - i - 1));
    }

    public static void main(String[] arg) {
        ArrayList<Integer> a = new ArrayList<>();
        a.add(1);
        a.add(2);
        a.add(3);
        System.out.println(a);

        reverse(a);
        System.out.println("reverse: " + a);
    }

}
