package pl.edu.uwm.wmii.jolantakazmierczak.laboratorium02;

public class Zadanie1d {
    public static void main(String[] arg) {
        int array[] = {-19, -18, -17, -16, -15, -14, -13, -12, 11, 9, 10, 8, 7, 6, 5, 4, 3, 2, 1, 0};
        int sum1 = 0, sum2 =0;
        for(int i = 0; i < array.length; i++){
            if(array[i] < 0)
                sum1 += array[i];
            if(array[i] > 0)
                sum2 += array[i];
        }
        System .out. println ("Suma liczb ujemnych: " + sum1 + "\nSuma liczb dodatnich: " + sum2);
    }
}

//obliczy sumę ujemnych elementów tablicy oraz sumę dodatnich elementów tablicy.