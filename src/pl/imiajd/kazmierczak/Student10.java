package pl.imiajd.kazmierczak;

import java.time.LocalDate;

public class Student10 extends Osoba10 implements Cloneable, Comparable{

    public Student10(String nazwisko, LocalDate dataUrodzenia, double sredniaOcen) {
        super(nazwisko, dataUrodzenia);
        this.sredniaOcen = sredniaOcen;
    }

    private double getSredniaOcen(){
        return sredniaOcen;
    }

    private double sredniaOcen;
}