package pl.imiajd.kazmierczak;

import java.time.LocalDate;

public class Skrzypce extends Instrument {
    public Skrzypce(String producent, LocalDate rokProdukcji) {
        super(producent, rokProdukcji);

    }
    @Override
    public String dzwiek() {
        return "dzwiek: " + dzwiek;
    }

    private String dzwiek = "g do g4 ";
}
