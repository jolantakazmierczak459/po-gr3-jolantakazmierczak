package pl.imiajd.kazmierczak;


import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class studentMap {
    public static void main(String[] args) {

        student();
    }
    public static void student() {
        Scanner n = new Scanner(System.in);
        n.useDelimiter("\n");
        Map<Object, String> map = new TreeMap<Object, String>();


        label:
        while (true) {
            System.out.println("Command list: \n1.add student\n2.remove\n3.change grade\n4.finish");
            System.out.println("Please enter a command: ");
            String todo = n.next();
            switch (todo) {
                case "add student":

                    System.out.println("Enter the grade");
                    String grade = n.next();

                    map.put(Students.addStudent(), grade);
                    break;
                case "remove":
                    System.out.println("Enter the id");
                    int remove = Integer.parseInt(n.next());

//                    map.remove(remove);
                    break;
                case "change grade":
                    System.out.println("Enter the surname");
                    String change = n.next();
                    System.out.println("Enter the new grade");
                    String newGrade = n.next();
//                    map.replace(change, newGrade);
                    break;
                case "finish":
                    System.out.println("Finish");
                    break label;
                default:
                    System.out.println("Wrong command");
                    break;
            }
            System.out.println("---------------------------");
            map.forEach((key, value) -> System.out.println(key + ": " + value));
            System.out.println("---------------------------");
        }
        System.out.println("---------------------------");
        map.forEach((key, value) -> System.out.println(key + ": " + value));
        System.out.println("---------------------------");
    }
}
class Students implements Comparable<Students>{
    String imie;
    String nazwisko;
    int id;

    public Students(String imie, String nazwisko, int id){
        this.imie = imie;
        this.nazwisko =  nazwisko;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return nazwisko + " " + imie + " " + id;
    }


    @Override
    public int compareTo(Students o) {
        return this.nazwisko.compareTo(o.nazwisko);
    }
    public static Students addStudent(){
        Scanner n = new Scanner(System.in);

        System.out.println("Enter the name");
        String name = n.next();
        System.out.println("Enter the surname");
        String surname = n.next();
        System.out.println("Enter the id");
        int id = Integer.parseInt(n.next());

        Students students = new Students(name, surname, id);
        return students;
    }
    public boolean ifcontains(Students o){
        if(this == o)
            return true;
        return false;
    }
}
