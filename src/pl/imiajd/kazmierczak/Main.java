package pl.imiajd.kazmierczak;



import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;

public class Main {
    public static void main(String[] args) {
        ArrayList<Komputer> grupa = new ArrayList<Komputer>();
        grupa.add(new Komputer("kom1", LocalDate.parse("2020-12-12")));
        grupa.add(new Komputer("kom1", LocalDate.parse("2020-12-13")));

        grupa.add(new Komputer("kom2", LocalDate.parse("2020-12-14")));
        grupa.add(new Komputer("kom6", LocalDate.parse("2020-12-14")));

        grupa.add(new Komputer("kom5", LocalDate.parse("2020-12-15")));


        System.out.println("-----------KOMPUTERY--------------");
        print(grupa);
        grupa.sort(new NameSorter());
        System.out.println("--------------------------------");
        print(grupa);

        ArrayList<Laptop> grupaLaptopow = new ArrayList<Laptop>();
        grupaLaptopow.add(new Laptop("lap1", LocalDate.parse("2020-12-12"), false));
        grupaLaptopow.add(new Laptop("lap1", LocalDate.parse("2020-12-13"), false));

        grupaLaptopow.add(new Laptop("lap2", LocalDate.parse("2020-12-14"), false));
        grupaLaptopow.add(new Laptop("lap3", LocalDate.parse("2020-12-14"), true));

        grupaLaptopow.add(new Laptop("lap5", LocalDate.parse("2020-12-14"), false));

        System.out.println("-----------LAPTOPY--------------");
        print(grupaLaptopow);
        System.out.println("--------------------------------");
        grupaLaptopow.sort(new NameSorter());
        print(grupaLaptopow);

        System.out.println("-----------TEST REDUKUJ--------------");

        LinkedList<Komputer> testRedukuj = new LinkedList<Komputer>();
        testRedukuj.add(new Komputer("kom1", LocalDate.parse("2020-12-12")));
        testRedukuj.add(new Komputer("kom2", LocalDate.parse("2020-12-13")));

        System.out.println("przed redukcją:");
        print(testRedukuj);
        redukuj(testRedukuj, 0);

        System.out.println("--------");
        System.out.println("po redukcji:");
        print(testRedukuj);

    }
    public static <T> void print(Iterable iterable) {
        Iterator iterator = iterable.iterator();

        ArrayList<String> list = new ArrayList<String>();

        while (iterator.hasNext()) {
            list.add(iterator.next().toString());
        }

        System.out.println(String.join("\n", list));
    }
    public static void redukuj(LinkedList<Komputer> komputery, int index) {
        if (index < 0 || index >= komputery.size()) {
            return;
        }

        komputery.remove(index);
    }
}
