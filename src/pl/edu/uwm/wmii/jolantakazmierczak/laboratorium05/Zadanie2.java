package pl.edu.uwm.wmii.jolantakazmierczak.laboratorium05;

import java.util.ArrayList;

public class Zadanie2 {
    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b) {
        ArrayList<Integer> wynik = new ArrayList<>();
        int rozmA = a.size();
        int rozmB = b.size();
        if (rozmA == rozmB) {
            for (int i = 0; i < 3; i++) {
                wynik.add(a.get(i));
                wynik.add(b.get(i));
            }
        } else if (rozmA > rozmB) {
            for (int i = 0; i < rozmB; i++) {
                wynik.add(a.get(i));
                wynik.add(b.get(i));
            }
            for (int i = rozmB; i < rozmA; i++) {
                wynik.add(a.get(i));
            }
        } else if (rozmA < rozmB) {
            for (int i = 0; i < rozmA; i++) {
                wynik.add(a.get(i));
                wynik.add(b.get(i));
            }
            for (int i = rozmA; i < rozmB; i++) {
                wynik.add(b.get(i));
            }
        }

        return wynik;
    }

    public static void main(String[] arg) {
        ArrayList<Integer> a = new ArrayList<>();
        a.add(1);
        a.add(2);
        a.add(3);
        ArrayList<Integer> b = new ArrayList<>();
        b.add(4);
        b.add(5);
        b.add(6);
        System.out.println(a);
        System.out.println(b);
        System.out.println("merge: " + merge(a, b));
    }

}