package pl.edu.uwm.wmii.jolantakazmierczak.laboratorium01.Zadanie2;

public class Zadanie4 {
    public static void main(String[] arg) {
        int n = 10;
        int[] tab = new int[n];
        tab[0] = -6;
        tab[1] = -4;
        tab[2] = -5;
        tab[3] = -1;
        tab[4] = 2;
        tab[5] = 3;
        tab[6] = 7;
        tab[7] = 0;
        tab[8] = 10;
        tab[9] = 15;

        int max = tab[0];
        for (int i = 1; i < tab.length; i++)
        {
            if (max < tab[i])
            {
                max = tab[i];
            }
        }
        int min = tab[0];
        for (int i = 1; i < tab.length; i++)
        {
            if (min > tab[i])
            {
                min = tab[i];
            }
        }

        System.out.println("Największa: " + max);
        System.out.println("Najmniejsza: " + min);
    }
}


//    Wczytać liczbę naturalną n, a następnie wczytując kolejno n liczb rzeczywistych znaleźć najmniejszą
//    oraz największą z wczytanych liczb.