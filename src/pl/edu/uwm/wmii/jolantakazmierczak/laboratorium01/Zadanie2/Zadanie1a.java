package pl.edu.uwm.wmii.jolantakazmierczak.laboratorium01.Zadanie2;

public class Zadanie1a {
    public static void main(String[] arg) {
        int n = 15;
        int[] tab = new int[n];
        for(int i=0; i<n; i++){
            tab[i] = i+1;
        }
        int suma = 0;
        for(int j=0; j<n; j++){
            if(tab[j] % 2 != 0){
                suma += 1;
            }
        }

        System .out. println (suma);
    }
}
