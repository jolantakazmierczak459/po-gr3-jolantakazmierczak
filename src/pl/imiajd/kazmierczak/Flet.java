package pl.imiajd.kazmierczak;

import java.time.LocalDate;

public class Flet extends Instrument {

    public Flet(String producent, LocalDate rokProdukcji) {
        super(producent, rokProdukcji);

    }
    @Override
    public String dzwiek() {
        return "dzwiek: " + dzwiek;
    }

    private String dzwiek = "c lub f";
}
