package pl.edu.uwm.wmii.jolantakazmierczak.laboratorium02;

import java.util.Scanner;

public class Zadanie1g {
    public static void main(String[] arg) {

        int array[] = {-19, -18, -17, 16, 15, -14, -13, -12, 11, 9, 10, 8, 7, 6, 5, -4, 3, 2, 1, 0};


        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj wartość dla lewy: ");
        int lewy = scanner.nextInt();
        System.out.print("Podaj wartość dla prawy: ");
        int prawy = scanner.nextInt();


        while (lewy < prawy) {
            int pomoc = array[lewy];
            array[lewy]  = array[prawy];
            array[prawy] = pomoc;

            lewy++;
            prawy--;
        }
        for(int i=0; i <= array.length; i++){
            System .out. println (array[i]);
        }


    }
}


//    wczyta dwie liczby całkowite do zmiennych lewy i prawy (1 ≤ lewy < n, 1 ≤ prawy <
//        n), a następnie odwróci kolejność elementów we fragmencie tablicy wyznaczonym
//        przez wartości tych zmiennych.