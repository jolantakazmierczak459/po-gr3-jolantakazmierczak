package pl.imiajd.kazmierczak;

import java.time.LocalDate;
import java.util.*;

public class TestOsoba
{
    public static void main(String[] args)
    {

        Osoba[] ludzie = new Osoba[2];

        ludzie[0] = new Pracownik("Kowalski", "Jan", LocalDate.parse("1989-11-01"), true, 50000, LocalDate.parse("2018-11-01"));
        ludzie[1] = new Student("Nowak", "Małgorzta", LocalDate.parse("2000-11-01"),false,"informatyka", 5.0);
        // ludzie[2] = new Osoba("Dyl Sowizdrzał");

        for (Osoba p : ludzie) {
            System.out.println(p.getImiona() + " " + p.getNazwisko() + " " +
                    p.jakaPlec() + " ur. " + p.getDataUrodzenia()+ " " + ": " + p.getOpis());
        }

    }
}








