package pl.edu.uwm.wmii.jolantakazmierczak.laboratorium01.Zadanie1;

public class Zadanie1a {
    public static void main(String[] arg) {
        int n = 15;
        int[] tab = new int[n];
        for(int i=0; i<n; i++){
            tab[i] = i;
        }
        int suma = 0;
        for(int j=0; j<n; j++){
            suma = suma + tab[j];
        }

        System .out. println (suma);
    }
}
