package pl.edu.uwm.wmii.jolantakazmierczak.laboratorium04;

import java.util.Scanner;

public class Zadanie1c {
    public static String middle(String str){
        int pozycja;
        int dl;
        if(str.length() % 2 ==0){
            pozycja = str.length() /2 - 1;
            dl = 2;
        }
        else {
            pozycja = str.length() /2;
            dl = 1;
        }
        return str.substring(pozycja, pozycja + dl);
    }
    public static void main(String[] arg) {
        System.out.println("Podaj napis:");
        Scanner input = new Scanner(System.in);
        String napis = input.nextLine();

        System.out.println("Wyraz środkowy: " + middle(napis));
    }
}
