package pl.edu.uwm.wmii.jolantakazmierczak.laboratorium02;

public class Zadanie1c {
    public static void main(String[] arg) {
        int array[] = {-19, -18, -17, -16, -15, -14, -13, -12, 11, 9, 10, 8, 7, 6, 11, 4, 3, 2, 11, 0};

        int max = array[0], count = 0;
        for (int i = 1; i < array.length; i++)
        {
            if (max < array[i])
            {
                max = array[i];
            }
        }

        for(int num: array){
            if(num == max)
                count++;
        }



        System .out. println ("Element największy w tablicy: " + max + " wystąpił " + count + " razy.");


    }
}




   // znajdzie w tablicy element największy oraz obliczy ile razy występuje on w tablicy.