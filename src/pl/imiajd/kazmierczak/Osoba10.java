package pl.imiajd.kazmierczak;


import java.time.LocalDate;

public class Osoba10 implements Cloneable, Comparable{
    public Osoba10(String nazwisko, LocalDate dataUrodzenia)
    {
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
    }

    public String getNazwisko() {
        return nazwisko;
    }
    public LocalDate getDataUrodzenia(){
        return dataUrodzenia;
    }

    public String toString(){
        String className = this.getClass().getSimpleName();
        return String.format("Klasa: %s  [nazwisko: %s - data urodzenia: %s] ", className, nazwisko, dataUrodzenia);
    }
//    nadpisz metodę equals z klasy Object, przy czym w porównaniu uwzględnij oba pola klasy.

    @Override
    public int compareTo(Object other) {

        boolean isDatesEqual = dataUrodzenia.compareTo(((Osoba10)other).dataUrodzenia) == 0;

        if (isDatesEqual) {
            return 1;
        } else {
            return -1;
        }
    }


    @Override
    public boolean equals(Object obj){
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Osoba10 other = (Osoba10) obj;
        if (nazwisko == null) {
            if (other.nazwisko != null)
                return false;
        } else if (!nazwisko.equals(other.nazwisko))
            return false;
        if (!dataUrodzenia.equals(other.dataUrodzenia))
            return false;
        return true;
    }


    private String nazwisko;
    private LocalDate dataUrodzenia;




}
