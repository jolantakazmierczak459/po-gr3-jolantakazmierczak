package pl.edu.uwm.wmii.jolantakazmierczak.test1;

import java.util.Scanner;

public class Zad1 {
    public static int ileW(int n) {
        int ilew = 0;
        for(int i=0; i< n; i++){
            if(i > 5){
                ilew++;
            }
        }
        return ilew;
    }
    public static int ileM(int n) {
        int ilem = 0;
        for(int i=0; i< n; i++){
            if(i < 5){
                ilem++;
            }
        }
        return ilem;
    }
    public static int ileR(int n) {
        int iler = 0;
        for(int i=0; i< n; i++){
            if(i == 5){
                iler++;
            }
        }
        return iler;
    }

    public static void main(String[] arg) {
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj liczbę");
        int n = in.nextInt();

        System.out.println("Ile większych: " + ileW(n));
        System.out.println("Ile mniejszych: " + ileM(n));
        System.out.println("Ile równych: " + ileR(n));

    }
}
