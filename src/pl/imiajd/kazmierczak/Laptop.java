package pl.imiajd.kazmierczak;

import java.time.LocalDate;

public class Laptop extends Komputer implements Cloneable, Comparable<Komputer> {
    private String nazwa;
    private LocalDate dataProdukcji;
    private boolean czyApple;

    public Laptop(String nazwa, LocalDate dataProdukcji, boolean czyApple) {
        super(nazwa, dataProdukcji);
        this.czyApple = czyApple;
    }
    public int compareTo(Laptop o) {
        int czyTakiSam = super.compareTo(o);

        if (czyTakiSam == 1 && (o.czyApple == this.czyApple)) {
            return 1;
        }

        return 0;
    }
    @Override
    protected Laptop clone() throws CloneNotSupportedException   {
        Laptop kopia = (Laptop) super.clone();
        kopia.czyApple = this.czyApple;

        return kopia;
    }
    @Override
    public boolean equals(Object o) {

        if (o == this) {
            return true;
        }

        if (!(o instanceof Laptop)) {
            return false;
        }

        if (((Laptop) o).nazwa != this.nazwa) {
            return false;
        }

        if (((Laptop) o).dataProdukcji != this.dataProdukcji) {
            return false;
        }

        if (((Laptop) o).czyApple != this.czyApple) {
            return false;
        }


        return true;
    }

}
