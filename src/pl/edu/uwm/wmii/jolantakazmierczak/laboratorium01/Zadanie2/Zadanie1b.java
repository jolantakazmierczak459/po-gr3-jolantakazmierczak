package pl.edu.uwm.wmii.jolantakazmierczak.laboratorium01.Zadanie2;

public class Zadanie1b {
    public static void main(String[] arg) {
        int n = 20;
        int[] tab = new int[n];
        for(int i=0; i<n; i++){
            tab[i] = i+1;
        }
        int suma = 0;
        for(int j=0; j<n; j++){
            if(tab[j] % 3 == 0 & tab[j] % 5 != 0){
                suma += 1;
            }
        }

        System .out. println (suma);
    }
}



//są podzielne przez 3 i niepodzielne przez 5
