package pl.imiajd.kazmierczak;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class TestOsoba10 {
    public static void main(String[] args) {

        ArrayList<Osoba10> grupa = new ArrayList<Osoba10>();
        grupa.add(new Osoba10("Kaźmierczak", LocalDate.parse("1999-11-05")));
        grupa.add(new Osoba10("Nowak", LocalDate.parse("1996-06-23")));
        grupa.add(new Osoba10("Nowak", LocalDate.parse("1989-11-01")));
        grupa.add(new Osoba10("Dąbrowski", LocalDate.parse("1989-11-01")));
        grupa.add(new Osoba10("Zabłocka", LocalDate.parse("1989-11-01")));


        for (Osoba10 s : grupa) {
            System.out.println(s.toString());
        }
        System.out.println("\n--------------------------sort--------------------------------\n");
        Collections.sort(grupa);


        for (Osoba10 s : grupa) {
            System.out.println(s.toString());
        }




    }




}
