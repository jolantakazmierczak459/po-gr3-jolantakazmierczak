package pl.edu.uwm.wmii.jolantakazmierczak.laboratorium01.Zadanie1;

public class Zadanie1g {
    public static void main(String[] arg) {
        int n = 5;
        int[] tab = new int[n];
        for(int i=0; i<n; i++){
            tab[i] = i+1;
        }
        int sumad = 0;
        int sumam = 1;
        for(int j=0; j<n; j++){
            sumad += tab[j];
            sumam *= tab[j];
        }

        System .out. println (sumad);
        System .out. println (sumam);
    }
}
