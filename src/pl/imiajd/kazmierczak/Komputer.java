package pl.imiajd.kazmierczak;

import java.time.LocalDate;

public class Komputer implements Cloneable , Comparable <Komputer> {

    private String nazwa;
    private LocalDate dataProdukcji;

    public Komputer(String nazwa, LocalDate dataProdukcji){
        this.nazwa = nazwa;
        this.dataProdukcji = dataProdukcji;
    }

    public String getNazwa() {
        return nazwa;
    }

    public LocalDate getDataProdukcji() {
        return dataProdukcji;
    }

    public void setDataProdukcji(LocalDate dataProdukcji) {
        this.dataProdukcji = dataProdukcji;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    @Override
    public boolean equals(Object o){
        if (o == this) {
            return true;
        }
        if (!(o instanceof Komputer)) {
            return false;
        }
        if(((Komputer) o).nazwa != this.nazwa){
            return false;
        }
        if(((Komputer) o).dataProdukcji != this.dataProdukcji){
            return false;
        }
        return true;
    }
    @Override
    public int compareTo(Komputer o) {

        if (o == this) {
            return 1;
        }

        if (o.nazwa != this.nazwa) {
            return 0;
        }

        if (o.dataProdukcji != this.dataProdukcji) {
            return 0;
        }

        return 1;
    }
    @Override
    protected Komputer clone() throws CloneNotSupportedException   {
        Komputer cloned = (Komputer) super.clone();
        cloned.dataProdukcji = LocalDate.from(this.dataProdukcji);

        return cloned;
    }

    @Override
    public String toString() {
        return nazwa + " " + dataProdukcji ;

    }
}
