package pl.imiajd.kazmierczak;

import java.time.LocalDate;
import java.util.ArrayList;

public class TestInstrumenty {
    public static void main(String[] args) {
        ArrayList<Instrument> orkiestra = new ArrayList<Instrument>();
        orkiestra.add(new Flet("Altus", LocalDate.parse("1989-11-01")));
        orkiestra.add(new Fortepian("Steinway", LocalDate.parse("1989-11-01")));
        orkiestra.add(new Flet("Hohner", LocalDate.parse("1989-11-01")));
        orkiestra.add(new Fortepian("Yamaha", LocalDate.parse("1989-11-01")));
        orkiestra.add(new Skrzypce("Stagg", LocalDate.parse("1989-11-01")));



        for (Instrument s : orkiestra) {
            System.out.println("producent: "+s.getProducent() + " " + s.dzwiek());
        }

    }
}