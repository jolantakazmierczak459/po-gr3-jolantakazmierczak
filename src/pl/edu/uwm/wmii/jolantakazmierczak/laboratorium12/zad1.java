package pl.edu.uwm.wmii.jolantakazmierczak.laboratorium12;

import java.util.Iterator;
import java.util.LinkedList;

public class zad1 {
    public static void main(String[] args)
    {
        LinkedList<String> list=new LinkedList<String>();

        list.add("Andrzej Maleszewski");
        list.add("Wojciech Kowalski");
        list.add("Jan Nowak");
        list.add("Marek Wieczorek");
        list.add("Anna Panna");
        list.add("Adam Małysz");
        list.add("Krzysztof Krawczyk");
        list.add("Grzegorz Brzęczyszczykiewicz");
        list.add("Alicja Nowakowska");

//        test redukuj
        redukuj(list, 2);

        Iterator<String> iterator=list.iterator();
        while(iterator.hasNext()){
            System.out.println(iterator.next());
        }

    }
    public static void redukuj(LinkedList<String> pracownicy, int n) {
        if (n <= 0) {
            throw new IllegalArgumentException("n musi być dodatni");
        }

        if (pracownicy == null) {
            throw new NullPointerException("Lista nie może być pusta");
        }

        for (int i = 0; i < pracownicy.size(); i++) {
            if (i % n == 0) {
                pracownicy.remove(i);
            }
        }
    }

}
