package pl.imiajd.kazmierczak;



import java.util.*;



public class lista12<T> {
    public static void main(String[] args)
    {
        LinkedList<String> list=new LinkedList<String>();

        list.add("Andrzej Maleszewski");
        list.add("Wojciech Kowalski");
        list.add("Jan Nowak");
        list.add("Marek Wieczorek");
        list.add("Anna Panna");
        list.add("Adam Małysz");
        list.add("Krzysztof Krawczyk");
        list.add("Grzegorz Brzęczyszczykiewicz");
        list.add("Alicja Nowakowska");

        System.out.println("-------------------------LAB 12-----------------------------------");


//        test redukuj
        redukuj(list, 2);
        System.out.println("Zad2 - test metody redukuj: ");
        System.out.println(list);
        System.out.println("------------------------------------------------------------------");

//        test odwroc
        odwroc(list);
        System.out.println("Zad4 - test metody odwroc: ");
        System.out.println(list);
        System.out.println("------------------------------------------------------------------");


//        test reverse
        System.out.println("Zad5 - test metody reverse: ");
        String zdanie = "Ala ma kota.  Jej kot lubi myszy.";
        System.out.println(zdanie);
        reverse(zdanie);
        System.out.println("------------------------------------------------------------------");

        System.out.println("Zad6 - test metody sReverse: ");
        sReverse(2015);
        System.out.println("------------------------------------------------------------------");

        System.out.println("Zad7 - test metody primes: ");
        primes(100);

        System.out.println("------------------------------------------------------------------");
        System.out.println("Zad8 ");
        BookList bookList = new BookList();
        bookList.addBook(new Book("Fajna książka"));
        bookList.addBook(new Book("Fajna książka 2"));

        print(bookList);

        LinkedList<String> bookList2=new LinkedList<String>();

        bookList2.add("Dobra książka");
        bookList2.add("Dobra książka 2");

        print(bookList2);

        AbstractList<String> bookList3 = new ArrayList<String>();

        bookList3.add("Książka 1");
        bookList3.add("Książka 2");
        bookList3.add("Książka 3");

        print(bookList3);

    }
    public static <T> void redukuj(LinkedList<T> pracownicy, int n) {
        Iterator<T> it = pracownicy.iterator();
        int rozmiar = pracownicy.size();
        for (int i = 0; i < rozmiar; i++) {
            it.next();
            if ((i + 1) % n == 0)
                it.remove();
        }
    }
    public static <T> void odwroc(LinkedList<T> lista){
        Collections.reverse(lista);
    }
    public static void reverse(String a) {
        Stack<String> stack = new Stack<>();
        String[] tablica = a.split("\\.");
        String temp = "";
        for (int j = 0; j < tablica.length; j++) {
            for (int i = 0; i < tablica[j].length(); i++) {
                if (tablica[j].charAt(i) == ' ') {
                    stack.push(temp);
                    temp = " ";

                } else {
                    temp += tablica[j].charAt(i);
                }
            }
            stack.push(temp);
            if (j > 0)
                stack.remove(0);

            int i = 0;
            while (!stack.isEmpty()) {
                String wyraz = stack.pop();
                if (i == 0) {
                    wyraz = wyraz.substring(1);
                    wyraz = wyraz.substring(0, 1).toUpperCase() + wyraz.substring(1);
                    System.out.print(wyraz);
                } else {
                    if (j == 0 && stack.size() == 0)
                        System.out.print(" ");
                    System.out.print(wyraz.toLowerCase());
                }
                i++;
            }
            System.out.print(". ");
        }
        System.out.println();

    }
    public static void sReverse(int a) {
        if (a >= 0) {
            Stack<Integer> stack = new Stack<Integer>();
            int rozmiar = String.valueOf(a).length();
            for (int i = 0; i < rozmiar; i++) {
                stack.push(a % 10);
                a = a / 10;
                System.out.print(stack.pop() + " ");
            }
        } else {
            System.out.println("Podaj liczbę nieujemną");
        }
        System.out.println();
    }
    public static void primes(int n) {
        int m = 2;
        ArrayList<Integer> primes = new ArrayList<>();
        while (m < n) {
            primes.add(m);
            m++;
        }

        for (int i = 2; i < n; i++) {
            for (int j = 0; j < primes.size(); j++) {
                if (primes.get(j) % i == 0 && primes.get(j) != i) {
                    primes.remove(j);
                }
            }
        }
        System.out.println(primes);
    }
    public static class Book {
        String name = "";

        public Book(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
    }
    public static class BookList implements Iterable<Book> {
        ArrayList<Book> bookList = new ArrayList<Book>();

        public BookList addBook(Book book) {
            bookList.add(book);
            return this;
        }

        @Override
        public Iterator<Book> iterator() {
            return bookList.iterator();
        }
    }
    public static <T> void print(Iterable iterable) {
        Iterator iterator = iterable.iterator();

        ArrayList<String> list = new ArrayList<String>();

        while (iterator.hasNext()) {
            list.add(iterator.next().toString());
        }

        System.out.println(String.join(", ", list));
    }
}
