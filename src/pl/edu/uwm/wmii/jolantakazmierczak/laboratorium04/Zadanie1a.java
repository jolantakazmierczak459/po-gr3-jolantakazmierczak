package pl.edu.uwm.wmii.jolantakazmierczak.laboratorium04;

import java.util.Scanner;

public class Zadanie1a {
    public static int countChar(String str, char c) {
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == c)
                count++;
        }
        return count;
    }

    public static void main(String[] arg) {
        System.out.println("Podaj text:");
        Scanner input = new Scanner(System.in);
        String text = input.nextLine();
        System.out.println(text);
        System.out.println("Podaj znak:");
        Scanner sc = new Scanner(System.in);
        char c = sc.next().charAt(0);

        System.out.println("Ilość: " + countChar(text, c));
    }
}
