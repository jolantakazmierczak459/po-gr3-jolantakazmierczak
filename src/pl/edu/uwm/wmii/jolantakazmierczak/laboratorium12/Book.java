package pl.edu.uwm.wmii.jolantakazmierczak.laboratorium12;
//zad 8
import pl.imiajd.kazmierczak.lista12;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

public class Book {
    String name = "";

    public Book(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
class BookList implements Iterable<Book> {
    ArrayList<Book> bookList = new ArrayList<Book>();

    public BookList addBook(Book book) {
        bookList.add(book);
        return this;
    }

    @Override
    public Iterator<Book> iterator() {
        return bookList.iterator();
    }
}

class App {
    public static void main(String[] args) {

        System.out.println("Zad8 ");
        lista12.BookList bookList = new lista12.BookList();
        bookList.addBook(new lista12.Book("Fajna książka"));
        bookList.addBook(new lista12.Book("Fajna książka 2"));

        print(bookList);

        LinkedList<String> bookList2=new LinkedList<String>();

        bookList2.add("Dobra książka");
        bookList2.add("Dobra książka 2");

        print(bookList2);

        AbstractList<String> bookList3 = new ArrayList<String>();

        bookList3.add("Książka 1");
        bookList3.add("Książka 2");
        bookList3.add("Książka 3");

        print(bookList3);
    }

    public static <T> void print(Iterable iterable) {
        Iterator iterator = iterable.iterator();

        ArrayList<String> list = new ArrayList<String>();

        while (iterator.hasNext()) {
            list.add(iterator.next().toString());
        }

        System.out.println(String.join(", ", list));
    }
}