package pl.edu.uwm.wmii.jolantakazmierczak.laboratorium01.Zadanie2;

public class Zadanie3 {
    public static void main(String[] arg) {
        int n = 10;
        int[] tab = new int[n];
        tab[0] = -6;
        tab[1] = -4;
        tab[2] = -5;
        tab[3] = -1;
        tab[4] = 2;
        tab[5] = 3;
        tab[6] = 7;
        tab[7] = 0;
        tab[8] = 10;
        tab[9] = 15;
        int dodatnie = 0;
        int ujemne = 0;
        int zera = 0;
        for (int i = 0; i < n; i++) {
            if (tab[i] > 0) {
                dodatnie += 1;
            } else if(tab[i] < 0){
                ujemne += 1;
            } else {
                zera += 1;
            }

        }

        System.out.println("Dodatnie: " + dodatnie);
        System.out.println("Ujemne: " + ujemne);
        System.out.println("Zera: " + zera);
    }
}

//    Wczytać liczbę naturalną n, a następnie wczytując kolejno n liczb rzeczywistych obliczyć ile jest
//        wśród nich liczb dodatnich, liczb ujemnych oraz ile jest zer.