package pl.edu.uwm.wmii.jolantakazmierczak.laboratorium01.Zadanie1;

public class Zadanie1i {
    public int silnia(int i)
    {
        if (i == 0)
            return 1;
        else
            return i * silnia(i - 1);
    }

    public static void main(String[] arg) {

        int n = 4;
        int[] tab = new int[4];
        tab[0] = 6;
        tab[1] = 4;
        tab[2] = 5;
        tab[3] = -1;

        Zadanie1i s = new Zadanie1i();

        double wynik = (tab[1])/s.silnia(1) + (tab[2])/s.silnia(2) - (tab[3])/s.silnia(3) + Math.pow(-1, n) * (tab[3])/s.silnia(n);


        System .out. println (wynik);


    }
}
