package pl.imiajd.kazmierczak;

import java.time.LocalDate;

class Student extends Osoba
{
    public Student(String nazwisko, String imiona, LocalDate dataUrodzenia, boolean plec, String kierunek, double sredniaOcen)
    {
        super(nazwisko, imiona, dataUrodzenia, plec);
        this.kierunek = kierunek;
        this.sredniaOcen = sredniaOcen;
    }
    @Override
    public String getOpis()
    {
        return String.format("kierunek studiów: %s średnia ocen: %.2f" , kierunek, sredniaOcen);
    }
    public double getSredniaOcen(){ return sredniaOcen; }

    public void setSredniaOcen(double newSrednia){
    this.sredniaOcen = newSrednia;
    }

    private String kierunek;
    private double sredniaOcen;
}
