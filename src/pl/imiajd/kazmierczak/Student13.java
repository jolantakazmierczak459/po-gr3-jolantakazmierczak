package pl.imiajd.kazmierczak;

import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Student13 {
    public static void main(String[] args) {

        student();
    }
    public static void student() {
        Scanner n = new Scanner(System.in);
        n.useDelimiter("\n");
        Map<String, String> map = new TreeMap<String, String>();


        label:
        while (true) {
            System.out.println("Command list: \n1.add student\n2.remove\n3.change grade\n4.finish");
            System.out.println("Please enter a command: ");
            String todo = n.next();
            switch (todo) {
                case "add student":
                    System.out.println("Enter the surname");
                    String name = n.next();
                    System.out.println("Enter the grade");
                    String grade = n.next();
                    map.put(name, grade);
                    break;
                case "remove":
                    System.out.println("Enter the surname");
                    String remove = n.next();
                    map.remove(remove);
                    break;
                case "change grade":
                    System.out.println("Enter the surname");
                    String change = n.next();
                    System.out.println("Enter the new grade");
                    String newGrade = n.next();
                    map.replace(change, newGrade);
                    break;
                case "finish":
                    System.out.println("Finish");
                    break label;
                default:
                    System.out.println("Wrong command");
                    break;
            }
            System.out.println("---------------------------");
            map.forEach((key, value) -> System.out.println(key + ": " + value));
            System.out.println("---------------------------");
        }
    }

}
