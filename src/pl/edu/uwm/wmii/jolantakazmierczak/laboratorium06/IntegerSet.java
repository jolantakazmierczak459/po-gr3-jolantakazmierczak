package pl.edu.uwm.wmii.jolantakazmierczak.laboratorium06;

import java.util.ArrayList;

public class IntegerSet {

    ArrayList<Boolean> list;

    public static void main(String[] args) {

        IntegerSet ob = new IntegerSet();

        //sprawdzenie union
        ArrayList<Boolean> unionResult = new ArrayList<Boolean>();
        unionResult = union(ob.list, ob.list);
        System.out.println(unionResult);

        //sprawdzenie intersection
        ArrayList<Boolean> intersectionResult = new ArrayList<Boolean>();
        intersectionResult = intersection(ob.list, ob.list);
        System.out.println(intersectionResult);

        //dodanie elementu
        ob.insertElement(true).insertElement(false);

        //usunięcie elementu
        ob.deleteElement(true);


        ArrayList<Boolean> testList = new ArrayList<Boolean>();
        testList.add(true);

        //porownanie elementu
        ob.equalsBase(testList);

        System.out.println(ob.equalsBase(testList));

        System.out.println(ob);


    }



    public IntegerSet() {
        int count = 150;
        this.list = new ArrayList<Boolean>();

        for (int index = 0; index < count; index++) {
            this.list.add(false);
        }
    }

    static ArrayList<Boolean> union(ArrayList<Boolean> list1, ArrayList<Boolean> list2) {
        ArrayList<Boolean> result = new ArrayList<Boolean>();
        for (int index = 0; index < list1.size(); index++) {
            result.add(getValue(index));
        }
        for (int index = 0; index < list2.size(); index++) {
            result.add(getValue(index));
        }

        return result;
    }

    static ArrayList<Boolean> intersection(ArrayList<Boolean> list1, ArrayList<Boolean> list2) {
        ArrayList<Boolean> result = new ArrayList<Boolean>();

        for (int index = 0; index < list1.size(); index++) {
            boolean val = getValue(index);
            if (list2.contains(val)) {
                result.add(val);
            }
        }

        return result;
    }

    static Boolean getValue(int index) {
        return index > 1 && index <= 100;
    }

    IntegerSet insertElement(boolean n) {
        list.add(n);
        return this;
    }

    IntegerSet deleteElement(boolean n) {
        list.remove(n);
        return this;
    }


    Boolean equalsBase(ArrayList<Boolean> list) {
        return list.containsAll(this.list);
    }

    public String toString() {
        String result = "";

        for (int index = 0; index < list.size(); index++) {
            result = result + index + " ";
        }

        return result;
    }
}
