package pl.edu.uwm.wmii.jolantakazmierczak.laboratorium01.Zadanie2;

public class Zadanie1c {
    public static void main(String[] arg) {
        int n = 25;
        int[] tab = new int[n];
        for(int i=0; i<n; i++){
            tab[i] = i+1;
        }
        int suma = 0;
        for(int j=0; j<n; j++){
            if(Math.sqrt(tab[j]) % 2 == 0){
                suma += 1;
            }
        }

        System .out. println (suma);
    }
}

//są kwadratami liczby parzystej