package pl.edu.uwm.wmii.jolantakazmierczak.laboratorium12;

import java.util.Iterator;
import java.util.LinkedList;

//Zmodyfikuj metodę z poprzedniego zadania, tak aby była ona metodą generyczną ze względu natyp elementów listy.


public class zad2<T> {
    public static void main(String[] args)
    {
        LinkedList<String> list=new LinkedList<String>();

        list.add("Andrzej Maleszewski");
        list.add("Wojciech Kowalski");
        list.add("Jan Nowak");
        list.add("Marek Wieczorek");
        list.add("Anna Panna");
        list.add("Adam Małysz");
        list.add("Krzysztof Krawczyk");
        list.add("Grzegorz Brzęczyszczykiewicz");
        list.add("Alicja Nowakowska");

//        test redukuj
        redukuj(list, 2);

        Iterator<String> iterator=list.iterator();
        while(iterator.hasNext()){
            System.out.println(iterator.next());
        }

    }
    public static <T> void redukuj(LinkedList<T> pracownicy, int n) {
        Iterator<T> it = pracownicy.iterator();
        int rozmiar = pracownicy.size();
        for (int i = 0; i < rozmiar; i++) {
            it.next();
            if ((i + 1) % n == 0)
                it.remove();
        }
    }

}
