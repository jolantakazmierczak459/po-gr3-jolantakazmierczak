package pl.imiajd.kazmierczak;

import java.time.LocalDate;

public class Fortepian extends Instrument {
    public Fortepian(String producent, LocalDate rokProdukcji) {
        super(producent, rokProdukcji);

    }
    @Override
    public String dzwiek() {
        return "dzwiek: " + dzwiek;
    }

    private String dzwiek = "od A2 = 27,50 Hz do c5 = ok. 4186,01 Hz";
}
