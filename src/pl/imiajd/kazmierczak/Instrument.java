package pl.imiajd.kazmierczak;

import java.time.LocalDate;

abstract class Instrument {
    public Instrument(String producent, LocalDate rokProdukcji)
    {
        this.producent = producent;
        this.rokProdukcji = rokProdukcji;
    }

    public LocalDate getRokProdukcji() {
        return rokProdukcji;
    }

    public String getProducent() {
        return producent;
    }
    public abstract String dzwiek();
    String producent;
    LocalDate rokProdukcji;
}
