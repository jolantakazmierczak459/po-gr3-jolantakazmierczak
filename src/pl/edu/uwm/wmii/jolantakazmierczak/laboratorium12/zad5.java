package pl.edu.uwm.wmii.jolantakazmierczak.laboratorium12;

import java.util.Stack;

public class zad5 {
    public static void main(String[] args) {
        String zdanie = "Ala ma kota.  Jej kot lubi myszy.";
        System.out.println(zdanie);
        reverse(zdanie);
//        System.out.println(reverse(zdanie));
//


    }

    public static void reverse(String a) {
        Stack<String> stack = new Stack<>();
        String[] tablica = a.split("\\.");
        String temp = "";
        for (int j = 0; j < tablica.length; j++) {
            for (int i = 0; i < tablica[j].length(); i++) {
                if (tablica[j].charAt(i) == ' ') {
                    stack.push(temp);
                    temp = " ";

                } else {
                    temp += tablica[j].charAt(i);
                }
            }
            stack.push(temp);
            if (j > 0)
                stack.remove(0);

            int i = 0;
            while (!stack.isEmpty()) {
                String wyraz = stack.pop();
                if (i == 0) {
                    wyraz = wyraz.substring(1);
                    wyraz = wyraz.substring(0, 1).toUpperCase() + wyraz.substring(1);
                    System.out.print(wyraz);
                } else {
                    if (j == 0 && stack.size() == 0)
                        System.out.print(" ");
                    System.out.print(wyraz.toLowerCase());
                }
                i++;
            }
            System.out.print(". ");
        }
        System.out.println();

    }
}
