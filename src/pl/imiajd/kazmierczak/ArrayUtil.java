package pl.imiajd.kazmierczak;

import java.time.LocalDate;
import java.util.Arrays;

class ArrayUtil<T>  {
    public static void main(String[] args)
    {
        System.out.println("Test Integer: ");
        Integer[] test = {9,10,4,5,7,8};
        System.out.println(ArrayUtil.isSorted(test));
        Arrays.sort(test);
        System.out.println("test isSorted() po sortowaniu: "+ArrayUtil.isSorted(test));
        System.out.println("test binSearch(): "+ArrayUtil.binSearch(test, 10));

        System.out.println("------------------------------------");

        System.out.println("Test LocalDate: ");
        LocalDate[] test2 ={LocalDate.parse("1989-11-01"), LocalDate.parse("1981-11-01")};
        System.out.println(ArrayUtil.isSorted(test2));
        Arrays.sort(test2);
        System.out.println("test isSorted() po sortowaniu: "+ArrayUtil.isSorted(test2));
        System.out.println("test binSearch(): "+ArrayUtil.binSearch(test2, LocalDate.parse("1981-11-01")));

        System.out.println("------------------------------------");
        System.out.println("Test selectionSort(): ");
        Integer[] test3 = {90,10,4,5,7,8};
        ArrayUtil.selectionSort(test3);
        System.out.println(ArrayUtil.isSorted(test3));
        LocalDate[] test4 ={LocalDate.parse("1989-11-01"), LocalDate.parse("1981-11-01")};
        ArrayUtil.selectionSort(test4);
        System.out.println(ArrayUtil.isSorted(test4));
    }




    public static <T extends Comparable> boolean isSorted (T[] array){
        for (int i = 0; i < array.length - 1; ++i) {
            if (array[i].compareTo(array[i + 1]) > 0)
                return false;
        }
        return true;
    }

    public static <T extends Comparable> int binSearch (T[] array, T ob){
        for(int i=0; i < array.length; i++){
            if(array[i].compareTo(ob) == 0)
                return i;
        }
        return -1;
    }
    public static  <T extends Comparable> void selectionSort (T[] array) {
        for (int i = 0; i < array.length; i++) {
            T min = array[i];
            int minId = i;
            for (int j = i+1; j < array.length; j++) {
                if (min.compareTo(array[j])>0) {
                    min = array[j];
                    minId = j;
                }
            }
            T temp = array[i];
            array[i] = min;
            array[minId] = temp;
        }
    }
    public static  <T extends Comparable> void mergeSort (T[] array) {

    }
}

