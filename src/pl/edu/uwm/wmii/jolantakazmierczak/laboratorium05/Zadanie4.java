package pl.edu.uwm.wmii.jolantakazmierczak.laboratorium05;

import java.util.ArrayList;

public class Zadanie4 {

    public static ArrayList<Integer> reversed(ArrayList<Integer> a) {
        ArrayList<Integer> wynik = new ArrayList<>();
        int rozmA = a.size();
        for (int i = 0; i < rozmA; i++) {
            wynik.add(a.get(rozmA - i - 1));
        }
        return wynik;
    }

    public static void main(String[] arg) {
        ArrayList<Integer> a = new ArrayList<>();
        a.add(1);
        a.add(2);
        a.add(3);
        System.out.println(a);

        System.out.println("reversed: " + reversed(a));
    }

}