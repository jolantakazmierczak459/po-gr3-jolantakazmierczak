package pl.edu.uwm.wmii.jolantakazmierczak.laboratorium01.Zadanie1;

public class Zadanie1d {
    public static void main(String[] arg) {

        int n = 4;
        double[] tab = new double[n];
        for(int i= 0; i<n; i++){
            tab[i] = i-5;
        }

        double suma = 0;

        for(int j=0; j<n; j++){
            suma = suma + Math.sqrt(Math.abs(tab[j]));
        }

        System .out. println (suma);
    }
}