package pl.imiajd.kazmierczak;


import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Scanner;

public class todo<T> {
    public static void main(String[] args) {

        toDo();
    }

    public static void toDo(){

            Queue<Zadanie> pOrder = new PriorityQueue<>();
            Scanner input = new Scanner(System.in);
            System.out.println();
            System.out.println("Command list: \n1.add priority description\n2.next\n3.finish");
            System.out.println("Please enter a command: ");

            outer:
            for (String text = input.nextLine(); ; text = input.nextLine()) {
                switch (text) {
                    case "add priority description":
                        System.out.println();

                        pOrder.add(Zadanie.addTask());
                        System.out.println("Task has been added");
                        break;
                    case "next":
                        System.out.println();
                        System.out.println("Most urgent task has been removed");
                        break;
                    case "finish":
                        System.out.println();
                        pOrder.poll();
                        System.out.println("Finish");
                        break outer;
                    default:
                        System.out.println();
                        System.out.println("Wrong command");
                }
            }

            System.out.println("----------------------------------");
            while (!pOrder.isEmpty()) {
                System.out.println(pOrder.poll());
            }
        }
    }

class Zadanie implements Comparable<Zadanie>{
    public PriorityQueue<Zadanie> theList;

    public int priority;
    public String description;

    public Zadanie (int priority, String description) {
        this.priority = priority;
        this.description = description;
    }
    @Override
    public int compareTo(Zadanie o) {
        return o.priority > this.priority ? -1: 1;
    }
    @Override
    public String toString() {
        return "Priority: " + this.priority + ", Description: " + this.description;
    }

    public static Zadanie addTask(){
        int prio;
        String desc;
        Scanner input = new Scanner(System.in);
        System.out.println("Enter a description of the new task:");
        desc = input.nextLine();

        System.out.println("Enter the priority of the new task:");
        prio = input.nextInt();



        Zadanie newTask = new Zadanie(prio, desc);
        return newTask;
    }
}