package pl.imiajd.kazmierczak;

public class Zadanie2 {
    public static void main(String[] args){

        Adres a = new Adres("warszawska", 81, 3, "Olsztyn", 11141);
        Adres b = new Adres("olsztyńska", 100, "Olsztyn", 11011);

        // test pokaz:
        a.pokaz();
        b.pokaz();

        //test przed:
        System.out.println(a.przed(b));
        System.out.println(b.przed(a));

    }
}
class Adres{
    public Adres(String ulica, int numer_domu, int numer_mieszkania, String miasto, int kod_pocztowy){
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.numer_mieszkania = numer_mieszkania;
        this.miasto = miasto;
        this.kod_pocztowy = kod_pocztowy;
    }
    public Adres(String ulica, int numer_domu, String miasto, int kod_pocztowy){
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.miasto = miasto;
        this.kod_pocztowy = kod_pocztowy;
    }
     void pokaz(){
        if (numer_mieszkania != 0)
        System.out.println(kod_pocztowy + " " + miasto + "\n"
        + ulica + " " + numer_domu  + "/" + numer_mieszkania);
        else
            System.out.println(kod_pocztowy + " " + miasto + "\n"
                    + ulica + " " + numer_domu);

    }

    public boolean przed(Adres a){
        if(kod_pocztowy > a.kod_pocztowy)
            return true;
            else
                return  false;

    }


    private String ulica;
    private int numer_domu;
    private int numer_mieszkania;
    private String miasto;
    private int kod_pocztowy;

}


