package pl.edu.uwm.wmii.jolantakazmierczak.laboratorium01.Zadanie2;

public class Zadanie5 {
    public static void main(String[] arg) {
        int n = 10;
        int[] tab = new int[n];
        tab[0] = -6;
        tab[1] = -4;
        tab[2] = -5;
        tab[3] = -1;
        tab[4] = 2;
        tab[5] = 3;
        tab[6] = 7;
        tab[7] = 0;
        tab[8] = 10;
        tab[9] = 15;
        for(int i=0; i<n-1; i++){
            if(tab[i] > 0 & tab[i+1] >0){
                System.out.println("(" + tab[i] + "," + tab[i+1] + ")");
            }
        }


    }
}


//    Wczytać liczbę naturalną n, a następnie wczytując kolejno n liczb rzeczywistych znaleźć ilość są-
//        siadujących par (a, b) takich, że a > 0 i b > 0. (Przykład: dla liczby n = 6 i kolejnych liczb
//        3, 5, 2, −4, 9, 7 poprawna odpowiedź to 3 (pary (3, 5), (5, 2) oraz (9, 7))).